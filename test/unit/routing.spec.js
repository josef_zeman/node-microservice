const { startApp } = require('./helpers/test-server-setup')
const {
  getTracerClient,
  getCacheClient,
  getSqlDBClient,
  getMqClient,
  getMiddleware,
  getApiClient,
  getUtils,
  errors
} = require('../../index')

const chai = require('chai')
const chaiHttp = require('chai-http');
const chaiAsPromised = require('chai-as-promised')
 
// Register the plugin
chai.use(chaiAsPromised)
chai.use(chaiHttp)

const expect = chai.expect

const { serviceSetup, mqListeners } = require('../config/service-setup')

const apiPath = 'http://localhost:3000';

describe('When we try to bootstrap an apifie service, with route enabled config setup', () => {
    before(async () => {
        const _setup = JSON.parse(JSON.stringify(serviceSetup))
        await startApp(_setup, mqListeners)
    })

    describe('is module is registering service api: - ', () => {
        it('GET Call', () => {            
            chai.request(apiPath).get('/testingRoute').end(function (err, res) {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res).to.be.an('object');
            });
        })

        it('POST Call', () => {            
            chai.request(apiPath).post('/testingRoute').end(function (err, res) {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res).to.be.an('object');
            });
        })

        it('PUT Call',() => {            
            chai.request(apiPath).put('/testingRoute').end(function (err, res) {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res).to.be.an('object');
            });
        })

        it('DELETE Call', () => {            
            chai.request(apiPath).delete('/testingRoute').end(function (err, res) {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res).to.be.an('object');
            });
        })
  })


  describe('is internal service is registering all routes: - ', () => {
    it('home(\'/\') route should return index HTML', () => {            
        chai.request(apiPath).get('/').end(function (err, res) {
            expect(err).to.be.null;
            expect(res).to.have.status(200);
            expect(res).to.be.html;
        });
    })

    it('(\'/ping\') route should return JSON', () => {            
        chai.request(apiPath).get('/ping').end(function (err, res) {
            expect(err).to.be.null;
            expect(res).to.have.status(200);
            expect(res).to.be.json;
        });
    })

    it('(\'/xmonitor/heartbeat\') route should return JSON', () => {            
        chai.request(apiPath).get('/xmonitor/heartbeat').end(function (err, res) {
            expect(err).to.be.null;
            expect(res).to.have.status(200);
            expect(res).to.be.json;
        });
    })

    it('(\'/xmonitor/stats\') route should return text', () => {            
        chai.request(apiPath).get('/xmonitor/stats').end(function (err, res) {
            expect(err).to.be.null;
            expect(res).to.have.status(200);
            expect(res.text).to.have.string('stats');
        });
    })


    it('(\'/xme/profile\') route should return JSON', () => {            
        chai.request(apiPath).get('/xme/profile').end(function (err, res) {
            expect(err).to.be.null;
            expect(res).to.have.status(200);
            expect(res).to.be.json;
        });
    })


    it('(\'/xci/newman\') route should return JSON', () => {            
        chai.request(apiPath).post('/xci/newman').end(function (err, res) {
            expect(err).to.be.null;
            expect(res).to.have.status(200);
            expect(res).to.be.json;
        });
    })


    it('(\'/xci/run-integration-tests\') route should return text when flag is off', () => {            
        chai.request(apiPath).get('/xci/run-integration-tests').end(function (err, res) {
            expect(err).to.be.null;
            expect(res).to.have.status(200);
            expect(res.text).to.have.string('Can\'t run integration tests');
        });
    })
})

})

