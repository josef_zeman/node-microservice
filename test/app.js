const cls = require('cls-hooked')

const namespace = cls.createNamespace('default')

const { serviceSetup, mqListeners } = require('./config/service-setup')
const { startApifieBlueprint, getUtils } = require('./../index')

let apifiedApp
console.log('namespace ', namespace.get('default'))

async function makeApifiedApp(app, generateApiDocs) {
  console.log('Requesting service bootstrap for your app')
  try {
    apifiedApp = await startApifieBlueprint(serviceSetup, mqListeners, app)
    if (generateApiDocs) {
      logger.info('Requesting generation of API Docs')
      await getUtils().generateApiDocs(serviceSetup.apiDocsPath)
    }
    return apifiedApp.app
  } catch (err) {
    console.error('Failed in makeApifiedApp %s ', err)
    throw err
  }
}

function getCache() {
  return apifiedApp.cache
}

function getMq() {
  return apifiedApp.mq
}

function getSqlDb() {
  return apifiedApp.sqlDb
}

function getTracer() {
  return apifiedApp.trace.tracer
}

function getRoutesMiddleware() {
  return apifiedApp.routes
}

function getApiMiddleware() {
  return apifiedApp.api
}

module.exports = {
  makeApifiedApp,
  cache: getCache,
  mq: getMq,
  sqlDb: getSqlDb,
  routes: getRoutesMiddleware,
  api: getApiMiddleware,
  tracer: getTracer
}
