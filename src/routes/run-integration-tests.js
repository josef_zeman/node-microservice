const express = require('express')
const swagger = require('swagger-spec-express')

const router = express.Router()
swagger.swaggerize(router)

const exec = require('child_process').exec

const { config } = require('../setup')

/* run integration tests */
router.get('/', (req, res) => {
  if (config.runIntegrationTests) {
    exec('npm run test:integrationall', (err, out, code) => {
      if (err) {
        res.status(500).send({
          code,
          out
        })
      } else {
        res.status(200).send(out)
      }
    })
  } else {
    res.status(200).send("Can't run integration tests")
  }
}).describe({
  tags: ['internal'],
  responses: {
    200: {
      description: 'run newman test cases'
    }
  }
})

module.exports = router
