const express = require('express')
const newmanTest = require('./newman')
const runIntegrationTests = require('./run-integration-tests')
const getConfig = require('./get-config')
const monitor = require('./monitor')

const swagger = require('swagger-spec-express')

const router = express.Router()
swagger.swaggerize(router)

/* GET home page. */
router.get('/', (req, res) => {
  res.render('index', { title: 'Express' })
}).describe({
  tags: ['internal'],
  responses: {
    200: {
      description: 'return simple express setup'
    }
  }
})

router.get('/ping', (req, res) => {
  res.status(200).json({ ping: 'pong' })
}).describe({
  tags: ['internal'],
  responses: {
    200: {
      description: 'this is ping return pong'
    }
  }
})

router.use('/xmonitor', monitor)
router.use('/xme/profile', getConfig)
router.use('/xci/newman', newmanTest)
router.use('/xci/run-integration-tests', runIntegrationTests)

module.exports = router
