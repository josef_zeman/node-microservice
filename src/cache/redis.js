const redis = require('async-redis')
const logger = require('./../trace/logger')
const { config } = require('../setup')
const InvalidConfig = require('../errors/invalid-config')

let redisClient
let redisStatus = {}

function checkConfig() {
  if (config.redisHost && (config.redisKey || config.redisPort)) {
    logger.info('Redis config is OK')
  } else {
    logger.error('All environment variables for redis not provided. Ensure REDIS_HOST, REDIS_PORT is properly set for both development and production, and REDIS_KEY only for production')
    throw new InvalidConfig('Missing configuration settings for connection Redis Client')
  }
}

async function startRedis() {
  checkConfig()
  logger.info('Starting Redis Client')

  let redisOptions = {}
  if (config.redisKey) {
    redisOptions = {
      auth_pass: `${config.redisKey}`,
      tls: {
        servername: `${config.redisHost}`
      }
    }
  }

  // Add your cache name and access key.
  try {
    redisClient = await redis.createClient(config.redisPort, config.redisHost, {
      ...redisOptions,
      retry_strategy(options) {
        if (options.total_retry_time > 1000 * 60 * 30) {
          // End reconnecting after a specific timeout and flush all commands
          // with a individual error
          logger.error('Redis Retry time exhausted')
          return new Error('Redis Retry time exhausted')
        }
        logger.error('Retry reconnect')
        // reconnect after
        return Math.min(options.attempt * 100, 3000)
      }
    })

    redisClient.on('error', (err) => {
      logger.error('Encountered Redis err', err)
    })

    redisClient.on('ready', () => {
      logger.info('Redis is ready')
      setRedisStatus({
        isActive: true
      })
    })

    redisClient.on('end', () => {
      logger.error('Redis connection lost')
      setRedisStatus({
        isActive: false
      })
    })

    logger.info('Redis client started. Key namespace = %s', `${config.redisKeyPrefix}_xxx`)
  } catch (err) {
    logger.info('Redis connect failed %j', err)
    throw err
  }
}

async function setRedisKey(key, value) {
  // this key will expire after 10 seconds. Protocol should be followed to prepend microservice name to keys
  await redisClient.set(`${config.redisKeyPrefix}_${key}`, value, 'EX', config.redisKeyTimeout)
}

async function getRedisKey(key) {
  const val = await redisClient.get(`${config.redisKeyPrefix}_${key}`)
  return val
}

async function deleteRedisKey(key) {
  // delete redis key
  await redisClient.del(`${config.redisKeyPrefix}_${key}`)
}

function setRedisStatus(status) {
  redisStatus = status
}

function getRedisStatus() {
  return redisStatus
}

module.exports = {
  startRedis,
  setRedisKey,
  getRedisKey,
  deleteRedisKey,
  getRedisStatus
}
