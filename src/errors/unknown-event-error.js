module.exports = class UnknownEvent extends Error {
  constructor(message) {
    super(message)
    this.name = this.constructor.name
    Error.captureStackTrace(this, this.constructor)
    Error.captureStackTrace(this, this.constructor)
  }
}
